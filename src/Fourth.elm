module Fourth exposing (..)
import Html exposing (..)
import Html.Events exposing (..)
import Browser
import Html.Attributes exposing (..)
import Set

main : Program () Model Msg
main =
  Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
  { content : String
  ,  output : String
  }


init : Model
init =
  { content = "", output = ""}



-- UPDATE


type Msg
  = Submitevent | Change String


update : Msg -> Model -> Model
update msg model =
  case msg of
    Change newContent ->
      { model | content = newContent }
    Submitevent ->
      { model | output = model.content }



-- VIEW



digitStr : List Char
digitStr = ['1','2','3','4']




valid : String -> Bool
valid str = (String.filter (\c -> not (Char.isDigit c)) str == "") && (String.length str == 4) && (Set.toList (Set.fromList (String.toList str)) == List.sort(String.toList str))





bullsNum : List Char -> String
bullsNum inp = 
  String.fromInt (
    List.sum (
      List.map2 
        (\x y -> 
        if x == y then
          1
        else 0)
    digitStr inp
    ))


cowsNum : List Char -> String
cowsNum inp = 
  String.fromInt (
    List.sum (
      List.map2 
        (\x y -> 
        if x /= y && (List.member y digitStr) then 
          1 
        else 0)
    digitStr inp
    ))


logic : String -> String
logic guess = 
  if valid guess then
    if bullsNum (String.toList guess) == "4" then
      "You won!"
    else bullsNum (String.toList guess) ++ " Bulls, " ++ cowsNum (String.toList guess) ++ " Cows"
  else "invalid"




view : Model -> Html Msg
view model =
  div []
    [ input [ placeholder "Enter guess", value (model.content), onInput Change ] []
    , button [onClick Submitevent] [text "submit"]
    , div [] [ text (logic model.output) ]
    ]