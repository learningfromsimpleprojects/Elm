module First exposing (..)
import Html exposing (..)
import Html.Events exposing (..)
import Browser



main : Program () Model Msg
main =
  Browser.sandbox { init = initialModel, update = update, view = view }



type alias Model
    = Int


initialModel : Model
initialModel =
    0


type Msg = Increment | Decrement | Reset

update : Msg -> Model -> Model
update msg model =
  case msg of
    Increment ->
      model + 1

    Decrement ->
      model - 1
    Reset -> 0



view : Model -> Html Msg

view model =
    div []
        [ button [ onClick Decrement ] [ text "-" ]
        , text (String.fromInt model)
        , button [ onClick Increment ] [ text "+" ]
        , button [ onClick Reset ] [text "Reset"]
        ]